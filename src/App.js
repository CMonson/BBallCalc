import React, { Component } from "react";
import { GameContext } from "./context/game-context";
import { normalize, schema } from "normalizr";
import Header from "./components/Header";
import defaultState from "./data/InitialState";
import Game from "./components/Game";
import "./App.css";

const player = new schema.Entity("players");
const team = new schema.Entity("teams", {
  players: [player]
});
const game = new schema.Entity("game", {
  teams: [team]
});

const normalizedState = normalize(defaultState, game);

const assistsHandler = (state, playerId) => {
  const stateCopy = { ...state };
  const playerCopy = stateCopy.entities.players[`${playerId}`];
  playerCopy.stats.ast = playerCopy.stats.ast + 1;
  stateCopy.entities.players[`${playerId}`] = {
    ...stateCopy.entities.players[`${playerId}`],
    ...playerCopy
  };
  return stateCopy;
};

const blocksHandler = (state, playerId) => {
  const stateCopy = { ...state };
  const playerCopy = stateCopy.entities.players[`${playerId}`];
  playerCopy.stats.blk = playerCopy.stats.blk + 1;
  stateCopy.entities.players[`${playerId}`] = {
    ...stateCopy.entities.players[`${playerId}`],
    ...playerCopy
  };
  return stateCopy;
};

const reducer = (state, action) => {
  switch (action.type) {
    case "ASSIST":
      return assistsHandler(state, action.playerId);
    case "BLOCK":
      return blocksHandler(state, action.playerId);
    default:
      return state;
  }
};

class App extends Component {
  state = {
    ...normalizedState,
    dispatch: action => {
      this.setState(state => reducer(state, action));
    }
  };

  render() {
    return (
      <GameContext.Provider value={this.state}>
        <Header />
        <Game />
        {/* Footer place holder */}
      </GameContext.Provider>
    );
  }
}

export default App;
