import React from "react";
import logo from "../bball.gif";
import { Container, Dropdown, Image, Menu, Icon } from "semantic-ui-react";

const SubMenu = props => {
  const { searchAction, addAction } = props;
  return (
    <Dropdown.Menu>
      <Dropdown.Item onClick={searchAction}>
        <Icon link name="search" />
        Search
      </Dropdown.Item>
      <Dropdown.Item onClick={addAction}>
        <Icon link color="green" name="add" />
        Add
      </Dropdown.Item>
    </Dropdown.Menu>
  );
};

const Header = props => {
  return (
    <div>
      <Menu fixed="top" inverted>
        <Container>
          <Menu.Item as="a" header>
            <Image size="mini" src={logo} style={{ marginRight: "1.5em" }} />
            Basketball Calculator
          </Menu.Item>
          <Menu.Item as="a">Home</Menu.Item>

          <Dropdown item simple text="Options">
            <Dropdown.Menu>
              <Dropdown.Item>
                <i className="dropdown icon" />
                <span className="text">Games</span>
                <SubMenu />
              </Dropdown.Item>
              <Dropdown.Item>
                <i className="dropdown icon" />
                <span className="text">Teams</span>
                <SubMenu />
              </Dropdown.Item>
              <Dropdown.Item>
                <i className="dropdown icon" />
                <span className="text">Players</span>
                <SubMenu />
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </Container>
      </Menu>
    </div>
  );
};

export default Header;
