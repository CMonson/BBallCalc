import React from "react";
import { GameContext } from "../context/game-context";

export default props => (
    <GameContext.Consumer>
      {({ entities, dispatch }) => {
        const { players } = entities;
        const player = players["1"];
        const { name } = player;
        return (
          <div>
            <h1>{name}</h1>
            <ul>
              {Object.entries(player.stats)
                .filter(([key, value]) => key !== "id")
                .map(([key, value]) => (
                  <li>{`${key}: ${value}`}</li>
                ))}
            </ul>
            <button onClick={() => dispatch({type: "ASSIST", playerId: player.id})}>+ Assist</button>
            <button onClick={() => dispatch({type: "BLOCK", playerId: player.id})}>+ Block</button>
          </div>
        );
      }}
    </GameContext.Consumer>
);
