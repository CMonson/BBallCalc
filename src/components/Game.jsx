import React from 'react';
import { Container } from 'semantic-ui-react';
import Player from './Player';

const Game = () => {
  return (
    <Container style={{marginTop: "7em"}}><Player /></Container>
  );
};

export default Game;
