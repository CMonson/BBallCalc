// Will eventually fetch this from an api.
export default {
  id: 1,
  teams: [
    {
      id: 1,
      location: "Salt Lake City",
      logo: "Some location on disc.",
      losses: 0,
      wins: 0,
      name: "TMB",
      players: [
        {
          id: 1,
          name: "Christopher Monson",
          number: 15,
          stats: {
            id: 1,
            ast: 0,
            blk: 0,
            dreb: 0,
            fta: 0,
            ftm: 0,
            min: 0,
            oreb: 0,
            pf: 0,
            pm: 0,
            pts: 0,
            stl: 0,
            thpa: 0,
            thpm: 0,
            to: 0,
            tpa: 0,
            tpm: 0
          }
        }
      ]
    }
  ]
};

// Normalized model =
/*{
  "entities": {
    "players": {
      "1": {
        "id": 1,
        "name": "Christopher Monson",
        "number": 15,
        "stats": {
          "id": 1,
          "ast": 0,
          "blk": 0,
          "dreb": 0,
          "fta": 0,
          "ftm": 0,
          "min": 0,
          "oreb": 0,
          "pf": 0,
          "pm": 0,
          "pts": 0,
          "stl": 0,
          "thpa": 0,
          "thpm": 0,
          "to": 0,
          "tpa": 0,
          "tpm": 0
        }
      }
    },
    "teams": {
      "1": {
        "id": 1,
        "location": "Salt Lake City",
        "logo": "Some location on disc.",
        "losses": 0,
        "wins": 0,
        "name": "TMB",
        "players": [
          1
        ]
      }
    },
    "game": {
      "1": {
        "id": 1,
        "teams": [
          1
        ]
      }
    }
  },
  "result": 1
}*/
